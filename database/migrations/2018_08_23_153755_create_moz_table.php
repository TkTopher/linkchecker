<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMozTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('moz', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->string('da');
            $table->string('pa');
            $table->string('moz_rank');
            $table->string('links_in');
            $table->string('equity');
            $table->string('alexa_rank');
            $table->string('a_links');
            $table->string('a_cnt');
            $table->string('a_cnt_rank');
            $table->string('sem_rush_domain');
            $table->string('sr_rank');
            $table->string('sr_kwords');
            $table->string('sr_traffic');
            $table->string('sr_costs');
            $table->string('sr_ulinks');
            $table->string('sr_hlinks');
            $table->string('sr_dlinks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('moz');
    }
}
