<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMozTableToAllowNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('moz', function(Blueprint $table)
        {
            $table->string('url')->nullable()->change();
            $table->string('da')->nullable()->change();
            $table->string('pa')->nullable()->change();
            $table->string('moz_rank')->nullable()->change();
            $table->string('links_in')->nullable()->change();
            $table->string('equity')->nullable()->change();
            $table->string('alexa_rank')->nullable()->change();
            $table->string('a_links')->nullable()->change();
            $table->string('a_cnt')->nullable()->change();
            $table->string('a_cnt_rank')->nullable()->change();
            $table->string('sem_rush_domain')->nullable()->change();
            $table->string('sr_rank')->nullable()->change();
            $table->string('sr_kwords')->nullable()->change();
            $table->string('sr_traffic')->nullable()->change();
            $table->string('sr_costs')->nullable()->change();
            $table->string('sr_ulinks')->nullable()->change();
            $table->string('sr_hlinks')->nullable()->change();
            $table->string('sr_dlinks')->nullable()->change();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
