<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('file', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('path');
            $table->unsignedInteger('moz_id')->nullable();
            $table->unsignedInteger('majestic_id')->nullable();
            $table->unsignedInteger('user_id');
            $table->string('moz_url')->nullable();
            $table->string('majestic_url')->nullable();
            $table->string('process_url')->nullable();
            $table->boolean('completed')->default(false);
            $table->string('moz_path')->nullable();
            $table->string('majestic_path')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('file');
    }
}
