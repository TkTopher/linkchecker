<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMajesticTableToAllowNull extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('majestic', function(Blueprint $table)
        {
            $table->string('status')->nullable()->change();
            $table->string('ebl')->nullable()->change();
            $table->string('ebl_edu')->nullable()->change();
            $table->string('ebl_gov')->nullable()->change();
            $table->string('refd')->nullable()->change();
            $table->string('refd_edu')->nullable()->change();
            $table->string('refd_gov')->nullable()->change();
            $table->string('ips')->nullable()->change();
            $table->string('ccs')->nullable()->change();
            $table->integer('tf')->nullable()->change();
            $table->integer('cf')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
