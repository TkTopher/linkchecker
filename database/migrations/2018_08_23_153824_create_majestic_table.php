<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMajesticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('majestic', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url')->index();
            $table->string('status');
            $table->string('ebl');
            $table->string('ebl_edu');
            $table->string('ebl_gov');
            $table->string('refd');
            $table->string('refd_edu');
            $table->string('refd_gov');
            $table->string('ips');
            $table->string('ccs');
            $table->integer('tf');
            $table->integer('cf');
            $table->string('ttft0')->nullable();
            $table->integer('ttfv0')->nullable();
            $table->string('ttft1')->nullable();
            $table->integer('ttfv1')->nullable();
            $table->string('ttft2')->nullable();
            $table->integer('ttfv2')->nullable();
            $table->string('ttft3')->nullable();
            $table->integer('ttfv3')->nullable();
            $table->string('ttft4')->nullable();
            $table->integer('ttfv4')->nullable();
            $table->string('ttft5')->nullable();
            $table->integer('ttfv5')->nullable();
            $table->string('ttft6')->nullable();
            $table->integer('ttfv6')->nullable();
            $table->string('ttft7')->nullable();
            $table->integer('ttfv7')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('majestic');
    }
}
