<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Digital Impact',
            'email' => 'services@digitalimpact.co.uk',
            'password' => bcrypt(env('ADMIN_PASSWORD'))
        ]);

        DB::table('users')->insert([
            'name' => 'Sidse',
            'email' => 'sidse@digitalimpact.co.uk',
            'password' => bcrypt(env('ADMIN_PASSWORD'))
        ]);

        DB::table('users')->insert([
            'name' => 'David',
            'email' => 'david@digitalimpact.co.uk',
            'password' => bcrypt(env('ADMIN_PASSWORD'))
        ]);

        DB::table('users')->insert([
            'name' => 'Emily',
            'email' => 'emily@digitalimpact.co.uk',
            'password' => bcrypt(env('ADMIN_PASSWORD'))
        ]);
    }
}
