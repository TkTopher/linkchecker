@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard <span class="pull-right" id="balance"></span></div>
                <div class="card-body">
                    @include('_partials.csvUpload')
                    <a href="{{ route('data.all') }}" class="btn btn-block btn-success mt-3"><span class="fa fa-file"></span> Processed Documents</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

<!--Ajax call to get the balance on page load-->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $.get("https://seo-rank.my-addr.com/balance.php?secret={{env("API_SECRET")}}", function(data, status){
            if(status !== "success")
                $("#balance").text("<b>Error</b>");
            $("#balance").text("$" + data);
        });
    });
</script>