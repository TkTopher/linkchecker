@extends('layouts.app')
@section('content')
    <div class="container">
        <h5>Files</h5>
        <form action="{{ route('data.find') }}" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <label>Select a file</label>
                <select name="file_id">
                    @foreach($files as $file)
                        @if($file->user_id == auth()->user()->id && $file->completed)
                            <option value="{{$file->id}}">{{$file->name}}</option>
                        @endif
                    @endforeach
                </select>
            </div>
            <button class="btn btn-success"><span class="fa fa-search"></span> Select File </button>
        </form>
        <small class="text-muted"><span class="fa fa-info"></span> Only files that have been processed will be available for viewing.</small>
    </div>
@endsection