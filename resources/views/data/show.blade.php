@extends('layouts.app')
@section('content')
    <div class="col-lg-10 mx-auto">
        <h5>Results <button id="dl" class="btn btn-sm btn-success" onclick="getIt()">Export CSV</button></h5>
        <div>
            <table class="table" id="myTable">
                <thead>
                <tr>
                    <th scope="col">URL</th>
                    <th scope="col">DA</th>
                    <th scope="col">PA</th>
                    <th scope="col">SR Traffic</th>
                    <th scope="col">TF</th>
                    <th scope="col">CF</th>
                    <th scope="col">Ratio</th>
                    <th scope="col">RefD</th>
                    <th scope="col">TTFT0</th>
                    <th scope="col">TTFV0</th>
                    <th scope="col">TTFT1</th>
                    <th scope="col">TTFV1</th>
                </tr>
                </thead>
                <tbody>
                @foreach($results as $result)
                    <tr>
                        <td>{{$result->url}}</td>
                        <td>{{$result->da}}</td>
                        <td>{{$result->pa}}</td>
                        <td>{{$result->sr_traffic}}</td>
                        <td>{{$result->tf}}</td>
                        <td>{{$result->cf}}</td>
                        <td>{{($result->tf > 0 && $result->cf > 0) ? ($result->tf / $result->cf) : 'N/A'}}</td>
                        <td>{{$result->refd}}</td>
                        <td>{{$result->ttft0}}</td>
                        <td>{{$result->ttfv0}}</td>
                        <td>{{$result->ttft1}}</td>
                        <td>{{$result->ttfv1}}</td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        </div>
    </div>


@endsection

@section('scripts')

    <script>
        $('#dl').click(function(){
            $("#myTable").first().table2csv();
        });
    </script>
@endsection

