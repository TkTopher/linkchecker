@if(count($errors) > 0)
    @foreach($errors->all() as $error)
        <div class="alert alert-danger">
            {{$error}}
            <i class="fa fa-exclamation-triangle"></i>
        </div>
    @endforeach
@endif

@if(session('success'))
    <div class="alert alert-success">
        {{session('success')}}
        <i class="fa fa-check-circle"></i>
    </div>
@endif

@if(session('error'))
    <div class="alert alert-danger">
        {{session('error')}}
        <i class="fa fa-exclamation-triangle"></i>
    </div>
@endif

@if(session('info'))
    <div class="alert alert-info">
        {{session('info')}}
        <i class="fa fa-question-circle"></i>
    </div>
@endif
