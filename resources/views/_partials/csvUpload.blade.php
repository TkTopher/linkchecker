<form method="POST" action="{{ route('file.store') }}" enctype="multipart/form-data">
    {{ csrf_field() }}
    <div class="form-group">
        <label>Provide File</label>
        <input type="file" class="form-control" name="linksFile" accept="text/csv">
    </div>
    <button class="btn btn-success"><span class="fa fa-upload"></span> Upload</button>
</form>

