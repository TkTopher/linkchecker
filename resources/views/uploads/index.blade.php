@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Dashboard</div>

                    <div class="card-body">
                        @foreach($files as $file)
                            <p>{{$file->name}} | Uploaded {{$file->created_at->diffForHumans()}}  <a href="{{route('file.repo', $file->id)}}" class="btn btn-sm btn-success">Process</a></p>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection