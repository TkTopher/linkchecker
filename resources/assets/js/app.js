
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

// window.Vue = require('vue');
window.TableFilter = require('tablefilter');
window.Table2CSV = require('table2csv');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

// Vue.component('example-component', require('./components/ExampleComponent.vue'));

var tf = new TableFilter('myTable', {
    base_path: 'https://unpkg.com/tablefilter@latest/dist/tablefilter/',
    // responsive: true,
    rows_counter: true,
    case_sensitive: true,
    watermark: ['URL', 'DA', 'PA', 'SR', 'TF', 'CF', 'Ratio', 'RefD', 'TTFT0', 'TTFV0', 'TTFT1', 'TTFV1'],
    mark_active_columns: {
        highlight_column: true
    },
    sticky_headers: true,
    loader: true,
    status_bar: true,
    btn_reset: {
        text: 'Reset'
    },
});
tf.init();


// const app = new Vue({
//     el: '#app'
// });
