<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');



Route::group(['prefix' => 'file', 'middleware' => 'auth'], function()
{
    Route::post('store', [
        'as' => 'file.store',
        'uses' => 'FileController@store'
    ]);

    Route::get('all', [
        'as' => 'file.all',
        'uses' => 'FileController@index'
    ]);

    Route::get('repo/{id}', [
        'as' => 'file.repo',
        'uses' => 'FileController@callRepo'
    ]);

    Route::get('process/{id}', [
        'as' => 'file.process',
        'uses' => 'FileController@process'
    ]);

});

Route::group(['prefix' => 'data', 'middleware' => 'auth'], function()
{
    Route::get('all', [
        'as' => 'data.all',
        'uses' => 'DataController@index'
    ]);

    Route::get('download/{filename}', [
        'as' => 'data.download',
        'uses' => 'DataController@download'
    ]);

    Route::post('data', [
        'as' => 'data.find',
        'uses' => 'DataController@show'
    ]);
});


