<?php

namespace App\Events;

use App\File;
use Illuminate\Broadcasting\Channel;
use Illuminate\Queue\SerializesModels;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;

class DownloadReadyEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;

    /**
     * Instance of the File Model.
     *
     * @var file
     */
    public $file;


    /**
     * @var user
     */
    public $user;


    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct($user, File $file)
    {
        $this->user = $user;
        $this->file = $file;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    public function broadcastOn()
    {
        return new PrivateChannel('channel-name');
    }
}
