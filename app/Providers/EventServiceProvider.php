<?php

namespace App\Providers;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'App\Events\DownloadReadyEvent' => [
            'App\Listeners\DownloadReadyListener',
        ],
        'App\Events\BigCsvDownloadReady' => [
            'App\Listeners\BigCsvDownloadReadyListener',
        ],
        'App\Events\FilesProcessed' => [
            'App\Listeners\FilesProcessedListener',
        ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        //
    }
}
