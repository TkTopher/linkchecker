<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    protected $guarded = [];
    protected $table = 'file';


    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function moz()
    {
        return $this->hasMany(Moz::class, 'file_id');
    }

    public function majestic()
    {
        return $this->hasMany(Majestic::class, 'file_id');
    }
}
