<?php

namespace App\Jobs;

use App\Events\FilesProcessed;
use App\File;
use App\Majestic;
use App\Moz;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Facades\Excel;

class ProcessCSV implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * The file to which the data being processed
     * is associated with.
     * @var integer
     */
    private $fileID;

    /**
     * The path of the Moz CSV to be processed.
     * @var string
     */
    private $mozPath;

    /**
     * The path of the Majestic CSV to be processed.
     * @var string
     */
    private $majesticPath;

    /**
     * Email address to notify on completion.
     * @var string
     */
    private $email;

    /**
     *
     * Create a new job instance.
     * @param $fileID integer
     * @param $mozPath string
     * @param $majesticPath string
     * @param $email string
     */
    public function __construct($fileID, $mozPath, $majesticPath, $email)
    {
        $this->fileID = $fileID;
        $this->mozPath = $mozPath;
        $this->majesticPath = $majesticPath;
        $this->email = $email;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $file = File::find($this->fileID);

        Excel::filter('chunk')->selectSheetsByIndex(0)->load($this->mozPath)->chunk(2500, function(Collection $csvLine) use($file){
            foreach($csvLine as $line)
            {
                if($line['url'] == 'Urls')
                    continue;

                Moz::create([
                    'url' => $line['url'],
                    'da' => $line['da'],
                    'pa' => $line['da'],
                    'moz_rank' =>  $line['mozrank'],
                    'links_in' =>  $line['linksin'],
                    'equity' =>  $line['equity'],
                    'alexa_rank' =>  $line['alexa_rank'],
                    'a_links' =>  $line['a_links'],
                    'a_cnt' =>  $line['a_cnt'],
                    'a_cnt_rank' =>  $line['a_cnt_rank'],
                    'sem_rush_domain' => $line['semrush_domain'],
                    'sr_rank' =>  $line['sr_rank'],
                    'sr_kwords' =>  $line['sr_kwords'],
                    'sr_traffic' =>  $line['sr_traffic'],
                    'sr_costs' =>  $line['sr_costs'],
                    'sr_ulinks' =>  $line['sr_ulinks'],
                    'sr_hlinks' =>  $line['sr_hlinks'],
                    'sr_dlinks' =>  $line['sr_dlinks'],
                    'file_id' => $file->id
                ]);
            }
        });

        Excel::filter('chunk')->selectSheetsByIndex(0)->load($this->majesticPath)->chunk(2500, function(Collection $csvLine) use($file) {
            foreach($csvLine as $line)
            {
                if($line['url'] == 'Urls')
                    continue;

                Majestic::create([
                    'url' => $line['url'],
                    'status' => $line['status'],
                    'ebl' => $line['ebl'],
                    'ebl_edu' => $line['ebl_edu'],
                    'ebl_gov' => $line['ebl_gov'],
                    'refd' => $line['refd'],
                    'refd_edu' => $line['refd_edu'],
                    'refd_gov' => $line['refd_gov'],
                    'ips' => $line['ips'],
                    'ccs' => $line['ccs'],
                    'tf' => $line['tf'],
                    'cf' => $line['cf'],
                    'ttft0' => $line['ttft0'],
                    'ttfv0' => $line['ttfv0'],
                    'ttft1' => $line['ttft1'],
                    'ttfv1' => $line['ttfv1'],
                    'ttft2' => $line['ttft2'],
                    'ttfv2' => $line['ttfv2'],
                    'ttft3' => $line['ttft3'],
                    'ttfv3' => $line['ttfv3'],
                    'ttft4' => $line['ttft4'],
                    'ttfv4' => $line['ttfv4'],
                    'ttft5' => $line['ttft5'],
                    'ttfv5' => $line['ttfv5'],
                    'ttft6' => $line['ttft6'],
                    'ttfv6' => $line['ttfv6'],
                    'ttft7' => $line['ttft7'],
                    'ttfv7' => $line['ttfv7'],
                    'file_id' => $file->id
                ]);
            }
        });

//        event(new FilesProcessed($this->email));
    }
}
