<?php

namespace App\Jobs;

use App\Events\BigCsvDownloadReady;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
use Carbon\Carbon;
use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;

class DownloadBigCSV implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public $timeout = 240;

    public $tries = 10;

    private $fileID;

    private $userEmail;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($fileID, $userEmail)
    {
        $this->fileID = $fileID;
        $this->userEmail = $userEmail;
    }

    /**
     * @throws \Box\Spout\Common\Exception\IOException
     * @throws \Box\Spout\Common\Exception\UnsupportedTypeException
     * @throws \Box\Spout\Writer\Exception\WriterNotOpenedException
     */
    public function handle()
    {

        $currentTime = str_random(16);
        $fileName = $currentTime;
        $header = array("url","da","pa","sr_traffic","tf","cf","refd","ttft0","ttfv0","ttft1","ttfv1");
        $filePath = public_path();

        $writer = WriterFactory::create(Type::CSV);
        $writer->openToFile($filePath.'/downloads/'.$fileName.'.csv');
        $writer->addRow($header);

        $data = [];
        DB::table('moz')
            ->join('majestic', 'moz.url', '=', 'majestic.url')
            ->where('moz.file_id', '=', $this->fileID)
            ->select('moz.url', 'moz.da', 'moz.pa', 'moz.sr_traffic', 'majestic.tf', 'majestic.cf', 'majestic.refd', 'majestic.ttft0', 'majestic.ttfv0', 'majestic.ttft1', 'majestic.ttfv1')
            ->orderBy('moz.url', 'asc')
            ->chunk(5000, function($table) use(&$data, $writer)
            {
                foreach($table as $item)
                {
                    $writer->addRow(get_object_vars($item));
                }
            });
        $writer->close();
        event(new BigCsvDownloadReady($fileName, $this->userEmail));
    }

    public function failed(\Exception $exception)
    {
        dd($exception);
    }
}
