<?php

namespace App\Listeners;

use App\Events\FilesProcessed;
use App\Notifications\FileProcessedNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class FilesProcessedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  FilesProcessed  $event
     * @return void
     */
    public function handle(FilesProcessed $event)
    {
        Notification::route('mail', $event->email)->notify(new FileProcessedNotification());

    }
}
