<?php

namespace App\Listeners;

use App\Events\DownloadReadyEvent;
use App\Notifications\DownloadReadyNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;

class DownloadReadyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  DownloadReadyEvent  $event
     * @return void
     */
    public function handle(DownloadReadyEvent $event)
    {
        Notification::route('mail', $event->user->email)->notify(new DownloadReadyNotification($event->user, $event->file));
    }
}
