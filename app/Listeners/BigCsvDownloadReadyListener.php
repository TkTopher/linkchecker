<?php

namespace App\Listeners;

use App\Events\BigCsvDownloadReady;
use App\Notifications\BigCsvDownloadNotification;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Notification;
use App\Notifications\DownloadReadyNotification;


class BigCsvDownloadReadyListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BigCsvDownloadReady  $event
     * @return void
     */
    public function handle(BigCsvDownloadReady $event)
    {
        Notification::route('mail', $event->email)->notify(new BigCsvDownloadNotification($event->filename));
    }
}
