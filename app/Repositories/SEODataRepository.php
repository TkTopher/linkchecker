<?php

namespace App\Repositories;

use App\File;
use Log;

class SEODataRepository {

    /**
     * File to be bulk processed.
     *
     * @var string
     */
    protected $filename;

    /**
     * DI API Secret
     *
     * @var string
     */
    protected $secret;

    /**
     * ID for Moz, Alexa and SemRush.
     * Set to 0 to initialize.
     *
     * @var int
     */
    protected $mozAlexaSemRushID = 0;

    /**
     * ID for Majestic.
     * Set to 0 to initialize.
     *
     * @var int
     */
    protected $majesticID = 0;

    /**
     * SEO Rank api endpoint for Moz, Alexa and SemRush bulk processing.
     */
    const API_ENDPOINT_MOZ_ALEXA_SEM = 'https://seo-rank.my-addr.com/upload_file.php?secret=';

    /**
     * SEO Rank api endpoint for Majestic bulk processing;
     */
    const API_ENDPOINT_MAJESTIC = 'https://seo-rank.my-addr.com/upload_file_majestic.php?secret=';

    /**
     * SEO Rank api endpoint for file retrieval after bulk processing.
     */
    const API_ENDPOINT_GET = 'https://seo-rank.my-addr.com/file_info.php?secret=7B3D6AC4A90B91466C34A74EA9BF2D97&id=';

    /**
     * Set to 1 to get data from the MOZ API,
     * set to 0 if not required.
     */
    const DO_MOZ = 1;

    /**
     * Set to 1 to get data from the ALEXA API,
     * set to 0 if not required.
     */
    const DO_ALEXA = 1;

    /**
     * Set to 1 to get data from the SEMRUSH API,
     * set to 0 if not required.
     */
    const DO_SEMRUSH = 1;


    /**
     * storage_path() wasn't returning the correct path,
     * as such this is needed. Change this in Prod
     */
//    const REAL_PATH = '/Applications/MAMP/htdocs/linkchecker/storage/app/';



    /**
     * SEODataRepository constructor.
     * @param $filename
     * @param $secret
     */
    public function __construct($filename, $secret)
    {
        $this->filename = $filename;
        $this->secret = $secret;
    }

    /**
     * The uploader for Moz, Alexa and SemRush bulk processing.
     *
     * @see https://seo-rank.my-addr.com/api_for_files.php
     */
    public function uploadMozAlexSemRushFile()
    {
        $file = File::where('path', $this->filename)->first();

        $f = curl_file_create(storage_path('/app/'). $this->filename);

        $settings['mainfile'] = $f;

        $path = $this->secret.'&filename='.$file.'&moz='.self::DO_MOZ.'&alexa='.self::DO_ALEXA.'&sr='.self::DO_SEMRUSH;

        $ch = curl_init(self::API_ENDPOINT_MOZ_ALEXA_SEM . $path);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $settings);

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            $error_msg = curl_error($ch);
            print_r($error_msg);
            dd($error_msg);
        }
        curl_close($ch);

        $this->mozAlexaSemRushID = $response;
        $this->storeDocumentIDs('moz', $this->filename, $response);
        $this->uploadMajesticFile($file);
    }

    /**
     * The uploader for Majestic bulk processing.
     *
     * @param $file
     */
    public function uploadMajesticFile($file)
    {
        $f = curl_file_create(storage_path('/app/') . $this->filename);
        $settings['mainfile'] = $f;

        $path = $this->secret.'&filename='.$file;

        $ch = curl_init(self::API_ENDPOINT_MAJESTIC . $path);

        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $settings);

        $response = curl_exec($ch);

        if (curl_error($ch)) {
            $error_msg = curl_error($ch);
            print_r($error_msg);
            dd($error_msg);
        }
        curl_close($ch);

        $this->majesticID = $response;
        $this->storeDocumentIDs('majestic', $this->filename, $response);

//        $this->getMozAlexSemRushFileInfo();
    }


    /**
     * Update the file in the db with the document ID for the API.
     *
     * @param $provider string
     * @param $path string
     * @param $id integer
     * @return bool
     */
    public function storeDocumentIDs($provider, $path, $id)
    {
        if($provider == 'moz')
        {
            $file = File::where('path', $path)->first();
            $file->moz_id = $id;
            $file->save();
            return true;
        }
        else if($provider == 'majestic')
        {
            $file = File::where('path', $path)->first();
            $file->majestic_id = $id;
            $file->save();
            return true;
        }
        return false;
    }

    /**
     * Check the API to get the file information back,
     * recursive code here to poll the API every 3 minutes due to
     * no webhooks being present.
     */
    public static function getMozAlexSemRushFileInfo($mozID)
    {
        $url = self::API_ENDPOINT_GET . $mozID;
        $string = file_get_contents($url);
        $test = list($filename,$lines,$processed,$status,$upload_timestamp,$flags,$link,$link_ranked) = explode('|',$string);
        return $link;
    }

    public static function getMajesticFileInfo($majesticID)
    {
        $url = self::API_ENDPOINT_GET . $majesticID;
        $string = file_get_contents($url);
        $test = list($filename,$lines,$processed,$status,$upload_timestamp,$flags,$link,$link_ranked) = explode('|',$string);
        return $link;
    }
}