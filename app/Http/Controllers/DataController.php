<?php

namespace App\Http\Controllers;

use App\File;
use App\Jobs\DownloadBigCSV;
use App\Majestic;
use App\Moz;
use Illuminate\Http\Response;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;


class DataController extends Controller
{
    public function index()
    {
        $files = File::all();
        return view('data.index', compact('files'));
    }

    public function show(Request $request)
    {
        $results = DB::table('moz')
        ->join('majestic', 'moz.url', '=', 'majestic.url')
        ->where('moz.file_id', '=', $request->file_id)
        ->select('moz.url', 'moz.da', 'moz.pa', 'moz.sr_traffic', 'majestic.tf', 'majestic.cf', 'majestic.refd', 'majestic.ttft0', 'majestic.ttfv0', 'majestic.ttft1', 'majestic.ttfv1')
        ->orderBy('moz.url', 'asc')
        ->get();


        if(count($results) > 8000)
        {
            DownloadBigCSV::dispatch($request->file_id, auth()->user()->email);
            return redirect()->back()->with('info', 'This file is too large to display all the data, a link will be emailed to you when your download is ready');
        }
        else
        {
            return view('data.show', compact('results'));
        }
    }

    public function download($filename)
    {
        return response()->download(public_path("downloads/{$filename}"));
    }

}
