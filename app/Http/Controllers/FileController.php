<?php

namespace App\Http\Controllers;

use App\File;
use App\Jobs\ProcessCSV;
use App\Majestic;
use App\Moz;
use App\Repositories\SEODataRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Storage;


class FileController extends Controller
{
    public function index()
    {
        $files = File::all();
        return view('uploads.index', compact('files'));
    }

    public function store(Request $request)
    {
        $file = $request->linksFile;
        $filename = str_random().'.'.$file->getClientOriginalExtension();
        Storage::putFileAs('/', $file, $filename);

        $uploadedFile = File::create([
            'name' => $file->getClientOriginalName(),
            'path' => $filename,
            'user_id' => auth()->id()
        ]);

        $seoRepo = new SEODataRepository($uploadedFile->path, env('API_SECRET'));
        $seoRepo->uploadMozAlexSemRushFile();

        return redirect('/home')->with('success', 'Your file has been uploaded for processing, you will receive an email shortly.');
    }

    /**
     * Ensure we have a valid file with valid download links,
     * then proceed to get the contents of the files.
     *
     * If valid, download and store into public directory for later processing,
     * else return user to homepage.
     *
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function process($id)
    {
        $file = File::find($id);

        if(! $file || !$file->moz_url || !$file->majestic_url) { return redirect('/home')->with('error', 'An error has occurred locating your file, please try again or re upload'); }

        $mozDownload = file_get_contents($file->moz_url);
        $majesticDownload = file_get_contents($file->majestic_url);

        $mozName = 'Moz_CSV_'. $file->id . '.csv';
        $majesticName = 'Majestic_CSV'. $file->id . '.csv';


        file_put_contents(public_path('downloads/'.$mozName), $mozDownload);
        file_put_contents(public_path('downloads/'.$majesticName), $majesticDownload);

        $mozPath = public_path('downloads/'.$mozName);
        $majesticPath = public_path('downloads/'.$majesticName);
        $email = auth()->user()->email;

        ProcessCSV::dispatch($file->id, $mozPath, $majesticPath, $email);

        return redirect('/home')->with('success', 'Your file has been downloaded and added to the queue for processing, you will receive an email shortly.');

    }
}
