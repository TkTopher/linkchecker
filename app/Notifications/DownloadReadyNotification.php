<?php

namespace App\Notifications;

use App\File;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class DownloadReadyNotification extends Notification
{
    use Queueable;

    /**
     * Instance of the User Model.
     *
     * @var user
     */
    public $user;

    /**
     * Instance of the File Model.
     *
     * @var file
     */
    public $file;


    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($user, File $file)
    {
        $this->user = $user;
        $this->file = $file;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Link Checker: SEO Notification')
            ->line('Hi, ' . $this->user->name . ' the next step in the link checking process is ready to begin.')
            ->line('To continue please click the link provided below.')
            ->action('Proceed to next step', 'http://linkchecker.test/file/process/'.$this->file->id);
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
