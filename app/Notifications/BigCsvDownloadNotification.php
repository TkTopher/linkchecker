<?php

namespace App\Notifications;

use App\File;
use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Support\Facades\Config;

class BigCsvDownloadNotification extends Notification
{
    use Queueable;

    /**
     * Instance of the File Model.
     *
     * @var file
     */
    public $filename;


    /**
     * BigCsvDownloadNotification constructor.
     * @param $filename
     */
    public function __construct($filename)
    {

        $this->filename = $filename;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject('Link Checker: SEO Notification')
            ->line('Hi, your CSV file is ready for download')
            ->line('To download your file, please click the link provided below.')
            ->action('Proceed to next step', 'http://linkchecker.test/data/download/'.$this->filename.'.csv');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
