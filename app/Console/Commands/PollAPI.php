<?php

namespace App\Console\Commands;

use App\Events\DownloadReadyEvent;
use App\File;
use App\Repositories\SEODataRepository;
use Illuminate\Console\Command;
use Log;

class PollAPI extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'seo:poll';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Poll the SEO Rank API for when the bulk processing is finished and a download is available for the CSV.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $allFiles = $this->getFiles();

        if(!count($allFiles))
        {
            Log::info('No files to process');
            return;
        }

        foreach($allFiles as $file)
        {
            if($file->completed == true)
            {
                continue;
            }

            if ($file->moz_url == null) {
                $link = SEODataRepository::getMozAlexSemRushFileInfo($file->moz_id);
                if ($link !== null) {
                    $file->moz_url = $link;
                    $file->save();
                }
            }

            if ($file->majestic_url == null) {
                $link = SEODataRepository::getMajesticFileInfo($file->majestic_id);
                if ($link !== null) {
                    $file->majestic_url = $link;
                    $file->save();
                }
            }

            if ($file->moz_url !== '' && $file->majestic_url !== '') {
                Log::info('Both Links ready!');
                $file->completed = 1;
                $file->save();
                event(new DownloadReadyEvent($file->user, $file));
            }
        }

    }

    public function getFiles()
    {
        $files = File::all();
        return $files;
    }
}
